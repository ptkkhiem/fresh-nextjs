---
title: Liên hệ
hide_title: false
sections:
  - section_id: contact-form
    type: section_form
    content: Nếu bạn muốn nói với tôi điều gì đó! Vui lòng điền vào biểu mẫu bên dưới.
    form_id: contactForm
    form_action: /thank-you
    form_fields:
      - input_type: text
        name: name
        label: Tên
        default_value: Nhập tên của bạn
        is_required: true
      - input_type: email
        name: email
        label: Địa chỉ Email
        default_value: Nhập địa chỉ Email của bạn
        is_required: true
      - input_type: select
        name: subject
        label: Chủ đề
        default_value: Vui lòng chọn
        options:
          - Blog bị lỗi
          - Tài trợ cho Blog
          - Vấn đề khác
      - input_type: textarea
        name: message
        label: Thông điệp của bạn!
        default_value: Nhập nội dung thông điệp!
      - input_type: checkbox
        name: consent
        label: >-
          Tôi hiểu rằng biểu mẫu này lưu trữ thông tin sẽ gửi của tôi để tôi có thể được liên hệ lại.
    submit_label: Gởi thông tin
seo:
  title: Liên hệ
  description: This is the contact page
  extra:
    - name: 'og:type'
      value: website
      keyName: property
    - name: 'og:title'
      value: Contact
      keyName: property
    - name: 'og:description'
      value: This is the contact page
      keyName: property
    - name: 'twitter:card'
      value: summary
    - name: 'twitter:title'
      value: Contact
    - name: 'twitter:description'
      value: This is the contact page
layout: advanced
---
